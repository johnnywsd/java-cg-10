
public class RayTracer4UFO extends MISApplet  {

	private static int WIDTH = 400;
	private static int HEIGHT = 400;
	private static int FOCAL = 300;

	//----- THESE TWO METHODS OVERRIDE METHODS IN THE BASE CLASS

	double[] v = new double[3];
	double[] w = new double[3];
	double[] s = { 200, 200, 200, 100 };	//x, y, z, r
	
	double[] t = new double[2];
	

	boolean solveQuadraticEquation(double A, double B, double C, double[] t) {

		double discriminant = B * B - 4 * A * C;
		if (discriminant < 0)
			return false;

		double d = Math.sqrt(discriminant);
		t[0] = (-B - d) / (2 * A);
		t[1] = (-B + d) / (2 * A);
		return true;
	}

	double tmpA = 0;
	double tmpB = 0;
	double tmpC = 0;
	
	double[] L = {10,-1,10,0,0,0,0,0,0,0};
	
	
	
	double square(double x){
		return x*x;
	}
	
	
	double[] tmpNomal = new double[3];
	double[] A = {0.3,0.3,0.0};
	double[] D = {1.0,1.0,0.0};
	double[] S = {1.0,1.0,1.0};
	double   p = 10;
	
	double[] A2 = {0.1,0.0,1.0};
	double[] D2 = {0.3,0.3,1.0};
	double[] S2 = {1.0,1.0,1.0};
	double   p2 = 10;
	
	private double[][][] lights={
			{ { 1.0, 1.0, 1.0}, {0.8, 0.8, 0.8} },
			{ {-1.0,-1.0,-1.0}, {0.8, 0.8, 0.8} },
	};
	
	Material material = new Material();
	Material material2 = new Material();
	
	GeoRay geo = null;
	GeoRay geo2 = null;
	GeoRay geo3 = null;
	
	Interset geoU = null;
	Union geoUU = null;
	
	boolean first = true;
	
	public void initFrame(double time) { // INITIALIZE ONE ANIMATION FRAME

		//       REPLACE THIS CODE WITH YOUR OWN TIME-DEPENDENT COMPUTATIONS, IF ANY.

		t_time = 10 * time;
		if(first){
		material.setAmbientColor(A);
		material.setDiffuseColor(D);
		material.setSpecularColor(S);
		material.setSpecularPower(p);
		
		material2.setAmbientColor(A2);
		material2.setDiffuseColor(D2);
		material2.setSpecularColor(S2);
		material2.setSpecularPower(p2);
		
		geo3 = new Globe();
		geo3.setMaterial(material2);
		geo3.matrix.scale(350,150,300);
		geo3.matrix.translate(200,130,200);
		geo3.transform();
		
		geo = new Globe();
		geo.setMaterial(material2);
//		double[] L = {-0.01,1,0.01,0,0,0,0,0,0,0};
//		geo.setL(L);
		geo.matrix.scale(80);
//		
		geo.matrix.translate(200,140,200);
		geo.transform();
		
		geo2 = new Globe();
		geo2.setMaterial(material2);
		geo2.matrix.scale(300,150,300);
		geo2.matrix.translate(200, 280, 200);
		geo2.transform();
		
		
		geoU = new Interset();
		geoU.setGeo1(geo2);
		
		geoU.setGeo2(geo3);
		geoU.setLight(lights);
//		geoU.matrix.identity();
//		
//		geoU.transform();
		geoUU = new Union();
		geoUU.setGeo1(geoU);
		geoUU.setGeo2(geo);
		geoUU.setLight(lights);
		
		first =false;
		}

	}
	
	double[] tmpS=new double[4];
	boolean flag1=false;
	boolean flag2=false;
	double[] tmpP1 = new double[3];
	double[] tmpP2 = new double[3];
	double[] tmpNomal2 = new double[3];
	private double tmpD = 0;
	private double tmpAttenuation = 0;
	private double constantK = 2;
	int[] fogColor = {240,240,240};
	int[] tmpRgb2 = new int[3];
	
	private double distant(double[] p1, double[] p2){
		double qsum = 0;
		for( int i=0;i<p1.length && i<p2.length ; i++){
			qsum += square(p1[i] - p2[i]);
		}
		return Math.sqrt(qsum);
	}
	
	void draw() {
		setPoint(v,200,200,-FOCAL);
		for(int i=0;i< WIDTH;i++){
			for(int j=0; j< HEIGHT;j++){
				setPoint(w,i-200,j-200,FOCAL);
				normalize(w,w);
				boolean flag4 = geoUU.raytrace(v, w, t, tmpP1, tmpP2,tmpNomal, tmpNomal2, rgbTmp, tmpRgb2);
				tmpD = distant(tmpP1, v);
				tmpAttenuation = Math.pow(2, -constantK*tmpD/300);
//				System.out.println(tmpAttenuation);
				
//				for(int c=0;c<3;c++){
//					rgbTmp[c] = (int) ((1-tmpAttenuation)*rgbTmp[c] + tmpAttenuation*fogColor[c]);
//				}
//				boolean flag4 = geoUU.raytrace(v, w, tmpP1,tmpNomal, rgbTmp);
//				System.out.println(flag4);
				if (flag4){
//					this.mapNomalToRgb(tmpNomal, material, dirE , rgbTmp);
					setPixel(i, j, rgbTmp);
				}else{
					rgbTmp[0] =(int) ( (double) j / HEIGHT * 140 );
					rgbTmp[1] = (int) ( 206 - (double)j/HEIGHT *(206-191));
					rgbTmp[2] = (int) ( 255 - (double)j/HEIGHT *(250-120));
//					tmpD = 150;
//					tmpAttenuation = Math.pow(2, -constantK*tmpD/300);
//					System.out.println(tmpAttenuation);
//					tmpAttenuation = 0.5;
//					for(int c=0;c<3;c++){
//						rgbTmp[c] = (int) ((1-tmpAttenuation)*rgbTmp[c] + tmpAttenuation*fogColor[c]);
//					}
					setPixel(i,j, rgbTmp);
				}
				

			}
		}
		


	}

	void setPoint(double[] point,double x,double y,double z){
		point[0] = x;
		point[1] = y;
		point[2] = z;
	}


	double t_time = 0;
	



	public void setLights(double [][][] lights){
		this.lights = lights;
	}

	@Override
	public void init(){
		super.init();

	}

	

	int rgbBak[] = {0,0,0};
	double centerX = 200;
	double centerY = 200;
	double  cwidth= 10;
	double space = 30;
	double r=0;
	double R =0;
	public void setPixel(int x, int y, int rgb[]) { // SET ONE PIXEL'S COLOR
		if(x>=0 && x<W && y>=0 && y<H){
			pix[x+y*W] = pack(rgb[0],rgb[1],rgb[2]);
		}

	}


	

	@Override
	public void computeImage(double time){
		initFrame(time); 
		draw();

	} 


	private int[] rgbTmp = new int[3];





	double tmp1 = 0;
	double tmp2 = 0;
	double[] Reflection = new double[3];
	double[] tmpRGB = new double[3];
	double[] Ldr;


	double tmpPoweredSum = 0;
	private void normalize(double[] src, double[] dst){
		tmpPoweredSum = 0;
		for(int i=0;i<src.length;i++){
			tmpPoweredSum += src[i]*src[i];
		}
		tmpPoweredSum = Math.sqrt(tmpPoweredSum);
		for(int i=0;i<src.length && i< dst.length;i++){
			dst[i] = src[i] / tmpPoweredSum;
		}
	}

}
