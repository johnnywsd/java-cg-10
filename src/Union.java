
public class Union extends GeoRay {

	GeoRay geo1 = null;
	GeoRay geo2 = null;
	public void setGeo1(GeoRay geo1){
		this.geo1 = geo1;
	}
	public void setGeo2(GeoRay geo2){
		this.geo2 = geo2;
	}
	

	
	public void setLight(double[][][] lights){
		super.setLight(lights);
	}
	public void transform(){
//		geo1.matrix.rightMultiply(this.matrix);
//		geo2.matrix.rightMultiply(this.matrix);
		geo1.matrix = this.matrix;
		geo2.matrix = this.matrix;
		geo1.transform();
		geo2.transform();
	}
	
	double[] tmpP1 = new double[3];
	double[] tmpP2 = new double[3];
	double[] tmpN1 = new double[3];
	double[] tmpN2 = new double[3];
	boolean flag1 = false;
	boolean flag2 = false;
	int[] tmpRgb11 = new int[3];
	int[] tmpRgb21 = new int[3];
	int[] tmpRgb12 = new int[3];
	int[] tmpRgb22 = new int[3];
	
	double[] tmpP11 = new double[3];
	double[] tmpP12 = new double[3];
	double[] tmpP21 = new double[3];
	double[] tmpP22 = new double[3];
	
	double[] tmpN11 = new double[3];
	double[] tmpN12 = new double[3];
	double[] tmpN21 = new double[3];
	double[] tmpN22 = new double[3];
	
	
	double[] t1 = new double[2];
	double[] t2 = new double[2];
 	
//	@Override
//	public boolean raytrace(
//			double[] v, double[] w, 
//			double[] P1, 
//			double[] normal1,
//			int[] rgb){
//		
//		
//		flag1 = geo1.raytrace(v, w, tmpP1, tmpN1,tmpRgb1);
//		flag2 = geo2.raytrace(v, w, tmpP2, tmpN2,tmpRgb2);
//		
//		if(flag1 && flag2){
//			if(tmpP1[2] < tmpP2[2]){
//				for(int i=0;i<3;i++){
//					P1[i] = tmpP1[i];
//					normal1[i] = tmpN1[i];
//					rgb[i] = tmpRgb1[i];
//				}
//			}else{
//				for(int i=0;i<3;i++){
//					P1[i] = tmpP2[i];
//					normal1[i] = tmpN2[i];
//					rgb[i] = tmpRgb2[i];
//				}
//			}
//		}
//		else if(flag1 && !flag2){
//			for(int i=0;i<3;i++){
//				P1[i] = tmpP1[i];
//				normal1[i] = tmpN1[i];
//				rgb[i] = tmpRgb1[i];
//			}
//		}
//		else if(!flag1 && flag2){
//			for(int i=0;i<3;i++){
//				P1[i] = tmpP2[i];
//				normal1[i] = tmpN2[i];
//				rgb[i] = tmpRgb2[i];
//			}
//		}
//		
//		
//		return flag1 || flag2;
//	}
	
	@Override
	public boolean raytrace(
			double[] v, double[] w, double[] t,
			double[] P1, double[] P2, 
			double[] normal1, double[] normal2,
			int[] rgb1, int[] rgb2){
		
		
		flag1 = geo1.raytrace(v, w, t1, tmpP11, tmpP12, tmpN11, tmpN12,tmpRgb11, tmpRgb12);
		flag2 = geo2.raytrace(v, w, t2, tmpP21, tmpP22, tmpN21, tmpN22,tmpRgb21, tmpRgb22);
		
		if(flag1 && flag2){
			if(tmpP11[2] < tmpP21[2]){
				for(int i=0;i<3;i++){
					P1[i] = tmpP11[i];
					normal1[i] = tmpN11[i];
					rgb1[i] = tmpRgb11[i];
				}
			}else{
				for(int i=0;i<3;i++){
					P1[i] = tmpP21[i];
					normal1[i] = tmpN21[i];
					rgb1[i] = tmpRgb21[i];
				}
			}
			
			if(tmpP12[2] > tmpP22[2]){
				for(int i=0;i<3;i++){
					P2[i] = tmpP12[i];
					normal2[i] = tmpN12[i];
//					rgb[i] = tmpRgb1[i];  we don't care the rgb of the back side
					rgb2[i] = tmpRgb12[i];
				}
			}else{
				for(int i=0;i<3;i++){
					P2[i] = tmpP22[i];
					normal2[i] = tmpN22[i];
//					rgb[i] = tmpRgb2[i];  we don't care the rgb of the back side
					rgb2[i] = tmpRgb22[i];
				}
			}
		}
		
		else if(flag1 && !flag2){
			for(int i=0;i<3;i++){
				P1[i] = tmpP11[i];
				normal1[i] = tmpN11[i];
				rgb1[i] = tmpRgb11[i];
				
				P2[i] = tmpP12[i];
				normal2[i] = tmpN12[i];
				rgb2[i] = tmpRgb12[i];
			}
		}
		
		else if(!flag1 && flag2){
			for(int i=0;i<3;i++){
				P1[i] = tmpP21[i];
				normal1[i] = tmpN21[i];
				rgb1[i] = tmpRgb21[i];
				
				P2[i] = tmpP22[i];
				normal2[i] = tmpN22[i];
				rgb2[i] = tmpRgb22[i];
			}
		}
		return flag1 || flag2;
	}

}
