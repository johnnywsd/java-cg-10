import java.util.Arrays;


public class GeoRay {


	private Matrix boundMatrix = null;
	private Material material = null;
	private double[][][] lights={
			{ { 1.0, 1.0, 1.0}, {0.8, 0.8, 0.8} },
			{ {-1.0,-1.0,-1.0}, {0.8, 0.8, 0.8} },
	};
	private double[] dirE = {0,0,1};
	
	public void setLight(double[][][] lights){
		this.lights = lights;
	}
	
	public GeoRay(){
		double[][] bound = {{-0.5,0.5},{-0.5,0.5},{-0.5,0.5}};
		boundMatrix = new Matrix(bound);	
		boundMatrix.toHomogeneousMatrix();
		
		material = new Material();
		double[] A = {0.2,0.0,0.0};
		double[] D = {0.8,0.0,0.0};
		double[] S = {1.0,1.0,1.0};
		double   p = 10;
		
		material.setAmbientColor(A);
		material.setDiffuseColor(D);
		material.setSpecularColor(S);
		material.setSpecularPower(p);
	}
	
	public void setMaterial(Material material){
		this.material = material;
	}
	
	public void setBound(double[][] bound){
		boundMatrix = new Matrix(bound);	
		boundMatrix.toHomogeneousMatrix();
	}
	public static void main(String[] args){
		double[] L = {1,1,1,0,0,0,0,0,0,-1};
		GeoRay geo = new GeoRay();
		geo.setL(L);
//		geo.matrix.rotateX(0.7);
		geo.matrix.scale(100,100,100);
		geo.matrix.translate(10, 10, 10);
		geo.transform();
		geo.showL();
		
		double[][] bound = {{-0.5,0.5},{-0.5,0.5},{-0.5,0.5}};
		geo.setBound(bound);
		geo.boundMatrix.rightMultiply(geo.matrix);
		geo.showBound();
	}
	
	private double[] L = null;
	private double[][] Q = new double[4][4];
	private Matrix QM = null;
	public Matrix matrix = new Matrix(4,4);
	
	public void showL(){
		for(int i=0;i<L.length;i++)
			System.out.print(L[i]+",  ");
		System.out.println("****");
	}
	
	public void showBound(){
		double [][] data = this.boundMatrix.getData();
		for(int i=0;i<data.length;i++){
			for(int j=0;j<data[0].length;j++){
				System.out.print(data[i][j]+",  ");
			}
			System.out.println();
		}
		
		System.out.println("**************");
			
		
	}
	
	public void setL(double[] L){
		matrix.identity();
		this.L = Arrays.copyOf(L, L.length);
		for (int i=0; i<4; i++)
			for(int j=0; j<4; j++){
				Q[i][j] = 0;
			}
		double a = L[0];
		double b = L[1];
		double c = L[2];
		double d = L[3];
		double e = L[4];
		double f = L[5];
		double g = L[6];
		double h = L[7];
		double i = L[8];
		double j = L[9];
		
		Q[0][0] = a;
		Q[0][1] = f;
		Q[0][2] = e;
		Q[0][3] = g;
		Q[1][1] = b;
		Q[1][2] = d;
		Q[1][3] = h;
		Q[2][2] = c;
		Q[2][3] = i;
		Q[3][3] = j;
		
		QM = new Matrix(Q);
	}
	
	public void transform(){
		double[][] invertMatrix = new double[4][4];
		Invert.invert(this.matrix.getData(), invertMatrix);
		Matrix invertM = new Matrix(invertMatrix);
		QM.rightMultiply(invertM.transpose());
		QM.leftMultiply(invertM);
		
		this.boundMatrix.rightMultiply(this.matrix);
		
		double upper = 0;
		double lower = 0;
		for(int i=0;i<3;i++){
			upper = Math.max(this.boundMatrix.getData()[i][0], this.boundMatrix.getData()[i][1]);
			lower = Math.min(this.boundMatrix.getData()[i][0], this.boundMatrix.getData()[i][1]);
			this.boundMatrix.getData()[i][0] = lower;
			this.boundMatrix.getData()[i][1] = upper;
		}
		
		this.Q2L();
	}
	
	private void Q2Upper(){
		for(int i=0; i<4; i++){
			for(int j=0;j<i;j++){
				QM.getData()[j][i] += QM.getData()[i][j];
				QM.getData()[i][j] = 0;
			}
		}
	}
	
	private void Q2L(){
		
		this.Q2Upper();
		double[][] Q = QM.getData();
		double a = Q[0][0];
		double f = Q[0][1];
		double e = Q[0][2];
		double g = Q[0][3];
		double b = Q[1][1];
		double d = Q[1][2];
		double h = Q[1][3];
		double c = Q[2][2];
		double i = Q[2][3];
		double j = Q[3][3];
		
		this.L[0] = a;
		this.L[1] = b;
		this.L[2] = c;
		this.L[3] = d;
		this.L[4] = e;
		this.L[5] = f;
		this.L[6] = g;
		this.L[7] = h;
		this.L[8] = i;
		this.L[9] = j;

	}
	
	double tmpA = 0;
	double tmpB = 0;
	double tmpC = 0;
	double[] tmpT = new double[2]; 
	boolean tmpFlag = false;
	double[] tmpP1 = new double[3];
	double[] tmpP2 = new double[3];
	
//	public boolean raytrace(
//			double[] v, double[] w, 
//			double[] P1, 
//			double[] normal1,
//			int[] rgb){
//		
//		double a = L[0];
//		double b = L[1];
//		double c = L[2];
//		double d = L[3];
//		double e = L[4];
//		double f = L[5];
//		double g = L[6];
//		double h = L[7];
//		double i = L[8];
//		double j = L[9];
//		
//		double wx=w[0];
//		double wy=w[1];
//		double wz=w[2];
//
//		double vx=v[0];
//		double vy=v[1];
//		double vz=v[2];
//		tmpA = a*square(wx) + b*square(wy) + c*square(wz)
//				+ d*wy*wz + e*wz*wx + f*wx*wy;
//		
//		tmpB = 2*(a*vx*wx + b*vy*wy + c*vz*wz)
//				+ d*(vy*wz + vz*wy) + e*(vz*wx + vz*wz) + f*(vx*wy + vy*wx)
//				+ g*wx + h*wy + i*wz;
//		
//		tmpC = a*square(vx) + b*square(vy) + c*square(vz)
//				+ d*vy*vz +e*vz*vx + f*vx*vy
//				+ g*vx +h*vy +i*vz
//				+ j;
//		
//		tmpFlag = solveQuadraticEquation(tmpA, tmpB, tmpC, tmpT);
//		
//		calPoint(v, w, tmpT[0], P1);
////		calPoint(v, w, tmpT[1], P2);
//		
//		double x = P1[0];
//		double y = P1[1];
//		double z = P1[2];
//		
//		boolean inbound = true;
//		
//		int index = 0;
////		double upper = Math.max(this.boundMatrix.getData()[index][0], this.boundMatrix.getData()[index][1]);
////		double lower = Math.min(this.boundMatrix.getData()[index][0], this.boundMatrix.getData()[index][1]);
//		while (index <3 && inbound){
//			inbound = inbound && (P1[index] > this.boundMatrix.getData()[index][0])
//					&& (P1[index] < this.boundMatrix.getData()[index][1] );
////			System.out.println(this.boundMatrix.getData()[index][0]);
//			index ++;
//		}
////		this.showBound();
//		
//		normal1[0] = 2*a*x + e*z + f*y + g;
//		normal1[1] = 2*b*y + d*z + f*x + h;
//		normal1[2] = 2*c*z + d*y + e*x + i;
//		
//		this.normalize(normal1, normal1);
//		
////		x = P2[0];
////		y = P2[1];
////		z = P2[2];
////		
////		normal2[0] = 2*a*x + e*z + f*y + g;
////		normal2[1] = 2*b*y + d*z + f*x + h;
////		normal2[2] = 2*c*z + d*y + e*x + i;
////		
////		this.normalize(normal2, normal2);
//		
////		System.out.println(tmpFlag && inbound);
////		return tmpFlag && inbound;
//		
//		this.mapNomalToRgb(normal1, material, dirE , rgb);
//		
//		return tmpFlag;
//		
//	}
	
	public boolean raytrace(
			double[] v, double[] w, double[] t,
			double[] P1, double[] P2, 
			double[] normal1, double[] normal2,
			int[] rgb1, int[] rgb2){
		
		double a = L[0];
		double b = L[1];
		double c = L[2];
		double d = L[3];
		double e = L[4];
		double f = L[5];
		double g = L[6];
		double h = L[7];
		double i = L[8];
		double j = L[9];
		
		double wx=w[0];
		double wy=w[1];
		double wz=w[2];

		double vx=v[0];
		double vy=v[1];
		double vz=v[2];
		tmpA = a*square(wx) + b*square(wy) + c*square(wz)
				+ d*wy*wz + e*wz*wx + f*wx*wy;
		
		tmpB = 2*(a*vx*wx + b*vy*wy + c*vz*wz)
				+ d*(vy*wz + vz*wy) + e*(vz*wx + vz*wz) + f*(vx*wy + vy*wx)
				+ g*wx + h*wy + i*wz;
		
		tmpC = a*square(vx) + b*square(vy) + c*square(vz)
				+ d*vy*vz +e*vz*vx + f*vx*vy
				+ g*vx +h*vy +i*vz
				+ j;
		
		tmpFlag = solveQuadraticEquation(tmpA, tmpB, tmpC, t);
		
		calPoint(v, w, t[0], P1);
		calPoint(v, w, t[1], P2);
		
		double x = P1[0];
		double y = P1[1];
		double z = P1[2];
		
		normal1[0] = 2*a*x + e*z + f*y + g;
		normal1[1] = 2*b*y + d*z + f*x + h;
		normal1[2] = 2*c*z + d*y + e*x + i;
		
		this.normalize(normal1, normal1);
		
		x = P2[0];
		y = P2[1];
		z = P2[2];
		
		normal2[0] = 2*a*x + e*z + f*y + g;
		normal2[1] = 2*b*y + d*z + f*x + h;
		normal2[2] = 2*c*z + d*y + e*x + i;
		
		this.normalize(normal2, normal2);
		
		
		this.mapNomalToRgb(normal1, material, dirE , rgb1);
		this.mapNomalToRgb(normal2, material, dirE , rgb2);
		
		return tmpFlag;
		
		
	}
	
	double square(double x){
		return x*x;
	}
	
	boolean solveQuadraticEquation(double A, double B, double C, double[] t) {

		double discriminant = B * B - 4 * A * C;
		if (discriminant < 0)
			return false;

		double d = Math.sqrt(discriminant);
		t[0] = (-B - d) / (2 * A);
		t[1] = (-B + d) / (2 * A);
		return true;
	}
	
	double tmpPoweredSum = 0;
	private void normalize(double[] src, double[] dst){
		tmpPoweredSum = 0;
		for(int i=0;i<src.length;i++){
			tmpPoweredSum += src[i]*src[i];
		}
		tmpPoweredSum = Math.sqrt(tmpPoweredSum);
		for(int i=0;i<src.length && i< dst.length;i++){
			dst[i] = src[i] / tmpPoweredSum;
		}
	}
	
	private void calPoint(double[] v, double[] w, double t, double[] p){
		for(int i=0;i< v.length && i<w.length && i<p.length; i++){
			p[i] = v[i] + w[i]*t;
		}
	}
	
	
	double tmp1 = 0;
	double tmp2 = 0;
	double[] Reflection = new double[3];
	double[] tmpRGB = new double[3];
	double[] Ldr;
	
	
	private void mapNomalToRgb(double[] N, Material material, double[] dirE, int[] rgb){
		for(int i=0;i<rgb.length;i++){
			tmpRGB[i] = 0;
		}

		double[] Argb = material.getAmbientColor();
		double[] Drgb = material.getDiffuseColor();
		double[] Srgb = material.getSpecularColor();
		double p = material.getSpecularPower();

		//light[1] = Lcolor, light[0] = Ldir
		for(double[][] light : this.lights){
			Ldr = light[0].clone();
			normalize(Ldr, Ldr);
			//			tmp1 = 2 * dotProduct(light[0], N);
			tmp1 = 2 * dotProduct(Ldr, N);
			for(int i=0;i<3;i++){
				//				Reflection[i] = tmp1*N[i] - light[0][i];
				Reflection[i] = tmp1*N[i] - Ldr[i];
			}
			//			tmp1 = Math.max(0, dotProduct(light[0],N) );
			tmp1 = Math.max(0, dotProduct(Ldr,N) );
			tmp2 = Math.pow(Math.max(0, dotProduct(Reflection, dirE)), p );

			for(int i=0;i<3;i++){
				tmpRGB[i] += light[1][i]*( Drgb[i] * tmp1 + Srgb[i] * tmp2);
			}

		}

		for(int i=0;i<3;i++){
			tmpRGB[i] += Argb[i];
		}
		gammaCorrection(tmpRGB,rgb,0.45);
	}
	
	private double dotProduct(double[] a, double[] b){
		double rtn = 0;
		for(int i=0;i<a.length && i< b.length;i++){
			rtn += a[i]* b[i];
		}
		return rtn;
	}

	
	private void gammaCorrection(double[] src, int[] dst, double arg){
		for(int i=0;i<3;i++){
			dst[i] = (int) (255* Math.pow(src[i], arg) );
		}
	}

}
