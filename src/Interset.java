
public class Interset extends GeoRay {

	GeoRay geo1 = null;
	GeoRay geo2 = null;
	public void setGeo1(GeoRay geo1){
		this.geo1 = geo1;
	}
	public void setGeo2(GeoRay geo2){
		this.geo2 = geo2;
	}
	

	
	public void setLight(double[][][] lights){
		super.setLight(lights);
	}
	public void transform(){
		geo1.matrix = this.matrix;
		geo2.matrix = this.matrix;
		geo1.transform();
		geo2.transform();
	}
	
	double[] tmpP1 = new double[3];
	double[] tmpP2 = new double[3];
	double[] tmpN1 = new double[3];
	double[] tmpN2 = new double[3];
	boolean flag1 = false;
	boolean flag2 = false;
	int[] tmpRgb11 = new int[3];
	int[] tmpRgb21 = new int[3];
	int[] tmpRgb12 = new int[3];
	int[] tmpRgb22 = new int[3];
	
	double[] tmpP11 = new double[3];
	double[] tmpP12 = new double[3];
	double[] tmpP21 = new double[3];
	double[] tmpP22 = new double[3];
	
	double[] tmpN11 = new double[3];
	double[] tmpN12 = new double[3];
	double[] tmpN21 = new double[3];
	double[] tmpN22 = new double[3];
	
	
	double[] t1 = new double[2];
	double[] t2 = new double[2];
 	
	private boolean isAsc(double a, double b, double c, double d){
		if(a<b && b<c && c<d){
			return true;
		}
		else{
			return false;
		}
	}
	
	private void assign(double[] P1, double[] PP1, double[] P2, double[] PP2,
			double[] normal1, double[] NN1, double[] normal2, double[] NN2,
			int[] rgb1, int[] rrgb1,int[] rgb2, int[] rrgb2){
		for(int i=0;i<3;i++){
			P1[i] = PP1[i];
			P2[i] = PP2[i];
			normal1[i] = NN1[i];
			normal2[i] = NN2[i];
			rgb1[i] = rrgb1[i];
			rgb2[i] = rrgb2[i];
		}
	}
	
	@Override
	public boolean raytrace(
			double[] v, double[] w, double[] t,
			double[] P1, double[] P2, 
			double[] normal1, double[] normal2,
			int[] rgb1, int[] rgb2){
		
		
		flag1 = geo1.raytrace(v, w, t1, tmpP11, tmpP12, tmpN11, tmpN12,tmpRgb11,tmpRgb12);
		flag2 = geo2.raytrace(v, w, t2, tmpP21, tmpP22, tmpN21, tmpN22,tmpRgb21,tmpRgb22);
		
		
		if(flag1 && flag2){
			if(isAsc(t2[0], t1[0], t2[1], t1[1])){
				assign(P1,tmpP12,P2,tmpP21,normal1,tmpN12,normal2,tmpN21,rgb1,tmpRgb12,rgb2,tmpRgb21);
			}
			else if(isAsc(t1[0], t2[0], t2[1], t1[1])){
				assign(P1,tmpP21,P2,tmpP22,normal1,tmpN21,normal2,tmpN22,rgb1,tmpRgb21,rgb2,tmpRgb22);
			}
			else if(isAsc(t1[0], t2[0], t1[1], t2[1])){
				assign(P1,tmpP21,P2,tmpP12,normal1,tmpN21,normal2,tmpN12,rgb1,tmpRgb21,rgb2,tmpRgb12);
			}
			else if(isAsc(t1[0], t1[1], t2[0], t2[1])){
				return false;
			}
			else if(isAsc(t2[0], t1[0], t1[1], t2[1])){
				assign(P1,tmpP11,P2,tmpP12,normal1,tmpN11,normal2,tmpN12,rgb1,tmpRgb11,rgb2,tmpRgb12);
			}
			else if(isAsc(t2[0], t2[1], t1[0], t1[1])){
				return false;
			}

		}
		
		return flag1 && flag2;
		
		
	}

}
