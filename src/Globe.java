
public class Globe extends GeoRay{

	static double[] L = {1,1,1,0,0,0,0,0,0,-1};
	static double[][] bound = {{-1, 1},{-1,1},{-1,1}};
//	static double[][] bound = {{-0.9, 0.9},{-0.9,0.9},{-0.9,0.9}};
	public Globe(){
		this.setL(L);
		this.setBound(bound);
	}

}
